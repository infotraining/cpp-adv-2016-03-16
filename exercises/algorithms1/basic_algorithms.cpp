#include <vector>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <cmath>
#include <numeric>
#include <random>

using namespace std;

int main()
{
    vector<int> vec(25);

    std::random_device rd;
    std::mt19937 mt {rd()};
    std::uniform_int_distribution<int> uniform_dist {1, 30};

    generate(vec.begin(), vec.end(), [&] { return uniform_dist(mt); } );

    cout << "vec: ";
    for(const auto& item : vec)
        cout << item <<" ";
    cout << endl;

    auto is_even = [](int x) { return x % 2 == 0; };

    // 1a - wyświetl parzyste
    // TODO for_each
    cout << "Parzyste: ";
    for_each(vec.begin(), vec.end(), [is_even](int x) { if (is_even(x)) cout << x << " ";});
    cout << endl;

    // 1b - wyswietl ile jest parzystych
    cout << "Ilosc parzystych: " << count_if(vec.begin(), vec.end(), is_even) << endl;

    const int eliminators[] = { 3, 5, 7 };
    // 2 - usuń liczby podzielne przez dowolną liczbę z tablicy eliminators
    // TODO remove_if
    auto garbage_begin =
            remove_if(vec.begin(), vec.end(),
                      [&eliminators](int x) {
                            return any_of(begin(eliminators), end(eliminators),
                                          [x](int e) { return x % e == 0; });});

    vec.erase(garbage_begin, vec.end());

    // 3 - tranformacja: podnieś liczby do kwadratu
    transform(vec.begin(), vec.end(), vec.begin(), [](int x) { return x * x; });
    cout << "Kwadraty: ";
    for(const auto& item : vec)
        cout << item << " ";
    cout << endl;


    // 4 - wypisz 5 najwiekszych liczb
    // TODO nth_element
    nth_element(vec.begin(), vec.begin() + 5, vec.end(), greater<int>());

    cout << "5 najwiekszych: ";
    for(auto it = vec.begin(); it != vec.begin() + 5; ++it)
        cout << *it << " ";
    cout << endl;

    // 5 - policz wartosc srednia
    // TODO accumulate / for_each
    auto avg = accumulate(vec.begin(), vec.end(), 0.0) / vec.size();

    // 6 - utwórz dwa kontenery - 1. z liczbami mniejszymi lub równymi średniej, 2. z liczbami większymi od średniej
    // TODO partition_copy
    vector<int> le_avg;
    vector<int> gt_avg;

    partition_copy(vec.begin(), vec.end(), back_inserter(gt_avg), back_inserter(le_avg),
                   [avg](int x) { return x > avg; });

    cout << "avg: " << avg << endl;

    cout << ">avg: ";
    for(const auto& item : gt_avg)
        cout << item << " ";
    cout << endl;

    cout << "<=avg: ";
    for(const auto& item : le_avg)
        cout << item << " ";
    cout << endl;
}
