#include "person.hpp"

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <functional>

using namespace std;

int main()
{
    vector<Person> employees;
    fill_person_container(employees);

    cout << "Wszyscy pracownicy:\n";
    copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));
    cout << endl;

    // wyświetl pracownikow z pensją powyżej 3000
    cout << "\nPracownicy z pensja powyzej 3000:\n";

    // wyświetl pracowników o wieku poniżej 30 lat
    cout << "\nPracownicy o wieku ponizej 30 lat:\n";

    // posortuj malejąco pracownikow wg nazwiska
    cout << "\nLista pracownikow wg nazwiska (malejaco):\n";

    // wyświetl kobiety
    cout << "\nKobiety:\n";

    // ilość osob zarabiajacych powyżej średniej
    cout << "\nIlosc osob zarabiajacych powyzej sredniej:\n";
}
