#include <iostream>
#include <cmath>
#include <array>
#include <sstream>
#include <vector>
#include <fstream>

using namespace std;

struct Vector2D
{
    double x, y;

    Vector2D(double x = 0.0, double y = 0.0) : x{x}, y{y}
    {}

    double length() const
    {
        return sqrt(x * x + y * y);
    }

    Vector2D operator+(const Vector2D& vec) const
    {
        return Vector2D(x + vec.x, y + vec.y);
    }

    Vector2D operator*(double a) const
    {
        return Vector2D(a * x, a * y);
    }

    Vector2D operator*(const Vector2D& vec) const
    {
        return Vector2D(x * vec.x , y * vec.y);
    }
};

Vector2D operator-(const Vector2D& v1, const Vector2D& v2)
{
    return Vector2D(v1.x - v2.x, v1.y - v2.y);
}

Vector2D operator-(const Vector2D& v)
{
    return Vector2D(-v.x, -v.y);
}

Vector2D operator*(double a, const Vector2D& vec)
{
    return vec * a;
}

constexpr int factorial(int n)
{
    return n == 0 ? 1 : factorial(n - 1) * n;
}

ostream& operator<<(ostream& out, const Vector2D& vec)
{
    out << "(" << vec.x << "," << vec.y << ")";

    return out;
}

istream& operator>>(istream& in, Vector2D& vec)  // (x,y) {x;y}
{
    double x, y;
    char lbracket = in.get();  // validate: (
    in >> x;
    char coma = in.get(); // valiadate: ,
    in >> y;
    char rbracket = in.get(); // validate: )

    vec.x = x;
    vec.y = y;

    return in;
}

int main(int argc, char *argv[])
{
    auto fact_4 = factorial(4);
    cout << "4! = " << fact_4 << endl;

    array<int, factorial(4)> arr;

    Vector2D vec1(1, 2);

    cout << "vec1: " << vec1 << endl;
    cout << "|vec1| = " << vec1.length() << endl;

    const Vector2D vec2(0, 4);

    auto vec3 = -vec1 + vec2;
    vec3 = vec1.operator +(vec2);
    cout << "vec3: " << vec3 << endl;

    auto vec4 = vec3 - vec1;  // operator-(vec3, vec1)
    cout << "vec4:" << vec4 << endl;

    auto vec5 = 2.0 * vec4;
    cout << "vec5: " << vec5 << endl;
    auto vec6 = vec1 * vec2;
    cout << "vec6: " << vec6 << endl;

    string number = to_string(10.2);

    stringstream ss;
    ss << vec6;
    string desc = ss.str();
    cout << "desc: " << desc << endl;

    istringstream iss("(1.0,3.5)");
    iss >> vec6;
    cout << "vec6: " << vec6 << endl;

    vector<Vector2D> data;
    fstream fin("vec.txt");

    if (!fin)
    {
        cout << "Problem z otwarciem pliku" << endl;
        exit(1);
    }

    while (fin)
    {
        string line;
        getline(fin, line);
        if (!fin)
            break;
        stringstream ss(line);
        Vector2D vec;
        ss >> vec;
        data.push_back(vec);
    }

    fin.close();

    cout << "data from file: \n";
    for(const auto& v : data)
        cout << v << endl;
}
