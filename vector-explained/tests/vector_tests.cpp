#define CATCH_CONFIG_MAIN

#include <algorithm>
#include "catch.hpp"
#include "../vector.hpp"

using namespace std;

TEST_CASE("vector constructed")
{

    SECTION("using default constructor")
    {
        Vector<int> vec;
        const Vector<int> const_vec;

        SECTION("size is zero")
        {
            REQUIRE(vec.size() == 0);
            REQUIRE(const_vec.size() == 0);
        }

        SECTION("is empty")
        {
            REQUIRE(vec.empty() == true);
            REQUIRE(const_vec.empty() == true);
        }
    }

    SECTION("using constructor with a size as argument")
    {
        Vector<int> vec(3);
        const Vector<int> const_vec(3);

        SECTION("size of vector is adjusted")
        {
            REQUIRE(vec.size() == 3);
        }

        SECTION("is not empty")
        {
            REQUIRE(vec.empty() == false);
        }

        SECTION("all items have a default value")
        {
            REQUIRE(all_of(vec.begin(), vec.end(), [](int x) { return x == int();}));
            REQUIRE(all_of(const_vec.begin(), const_vec.end(), [](int x) { return x == int();}));
        }
    }

    SECTION("using constructor with a size as argument and a default value")
    {
        Vector<int> vec(3, -1);

        SECTION("size of vector is adjusted")
        {
            REQUIRE(vec.size() == 3);
        }

        SECTION("all items have a given value")
        {
            REQUIRE(all_of(vec.begin(), vec.end(), [](int x) { return x == -1;}));
        }
    }

    SECTION("using constructor with initializer list")
    {
        Vector<int> vec{1, 2, 3};
        Vector<double> vec_d{1, 2, 3};

        SECTION("size is adjusted")
        {
            REQUIRE(vec.size() == 3);
        }

        SECTION("all items are initialized with values from il")
        {
            auto expected = { 1, 2, 3 };  // std::initializer_list<int>

            REQUIRE(equal(vec.begin(), vec.end(), expected.begin()));
            REQUIRE(equal(vec_d.begin(), vec_d.end(), expected.begin()));
        }
    }
}

TEST_CASE("iterators", "[vector]")
{
    Vector<int> vec1(3);

    SECTION("can be passed to algorithm")
    {
        Vector<int>::iterator start = vec1.begin();
        fill(start, vec1.end(), 1);

        REQUIRE(all_of(vec1.begin(), vec1.end(), [](int x) { return x == 1;}));
    }
}

TEST_CASE("indexing", "[vector]")
{
    Vector<int> vec(3, -1);
    const Vector<int> const_vec = {1, 2, 3};

    SECTION("can assign value using index")
    {
        vec[0] = 1;
        REQUIRE(*vec.begin() == 1);
    }

    SECTION("can read value using index")
    {
        REQUIRE(vec[0] == -1);
        REQUIRE(const_vec[0] == 1);
    }
}

TEST_CASE("copying")
{
    Vector<int> vec = { 1, 2, 3 };

    SECTION("vector is copyable")
    {
        Vector<int> vec2 = vec; // copy constructor

        SECTION("all items are copied when copy constructed")
        {
            auto expected = { 1, 2, 3 };
            vec[0] = -1;
            REQUIRE(equal(vec2.begin(), vec2.end(), expected.begin()));
        }

        SECTION("all items are copied when assigned")
        {
            auto expected = { 7, 8, 9 };
            Vector<int> new_vec(expected);
            vec2 = new_vec;

            new_vec[0] = -1;

            REQUIRE(equal(vec2.begin(), vec2.end(), expected.begin()));
        }
    }
}

// move semantics

Vector<int> load_data(size_t size)
{
    Vector<int> vec(size);

    for(size_t i = 0; i < size; ++i)
        vec[i] = i;

    return vec;
}

TEST_CASE("move")
{

    SECTION("rvalues vs lvalue")
    {
        int x = 10;
        int& rx = x;  // l-value ref
        rx = 3;
        REQUIRE(x == 3);

        const Vector<int>& ref = load_data(10);  // const l-value ref

        Vector<int>&& rref = load_data(10); // r-value ref

        Vector<int> data = load_data(2000);  // kopiowanie
    }

    SECTION("explicit move")
    {
        Vector<int> vec1 = { 1, 2, 3, 4 };
        Vector<int> vec2 = { 6, 7 };

        vec1 = move(vec2);

        auto expected = { 6, 7 };

        REQUIRE(vec1.size() == 2);
        REQUIRE(equal(vec1.begin(), vec1.end(), expected.begin()));
        REQUIRE(vec2.size() == 0);
    }
}

class Data
{
    Vector<int> data_;
public:
    Data() = default;

//    ~Data()
//    {
//        cout << "~Data()" << endl;
//    }

//    Data(Data&& source) : data_{move(source.data_)}
//    {
//    }

//    Data& operator=(Data&& source)
//    {
//        if (this != &source)
//        {
//            data_ = move(source.data_);
//        }

//        return *this;
//    }

    void load_from_file(const string& filename)
    {
        data_ = load_data(100);
    }

    const Vector<int>& data() const
    {
        return data_;
    }
};

TEST_CASE("move for data")
{
    Data data;
    data.load_from_file("test");

    Data new_data = move(data);
}


