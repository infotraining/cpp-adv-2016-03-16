#ifndef CLASS_TEMPLATES_VECTOR_HPP
#define CLASS_TEMPLATES_VECTOR_HPP

#include <cstddef>
#include <iostream>

template <typename T>
class Vector
{
    T* items_; // dynamic array
    size_t size_;

public:
    using iterator = T*;
    using const_iterator = const T*;

    Vector() : items_{nullptr}, size_{0}
    {}

    Vector(size_t size, const T& value = T()) : items_{new T[size]}, size_{size}
    {
        std::fill_n(items_, size_, value);
    }

    Vector(std::initializer_list<T> il) : items_{new T[il.size()]}, size_{il.size()}
    {
        std::copy(il.begin(), il.end(), begin());
    }

    // konstruktor kopiujący
    Vector(const Vector& source) : items_{new T[source.size()]}, size_{source.size()}
    {
        std::cout << "Copy ctor of vector" << std::endl;
        std::copy(source.begin(), source.end(), items_);
    }

    // konstruktor przenoszący - move constructor
    Vector(Vector&& source) : items_{source.items_}, size_{source.size_}
    {
        //std::cout << "Move ctor of vector" << std::endl;
        source.items_ = nullptr;
        source.size_ = 0;
    }

    Vector& operator=(const Vector& source)
    {
        if (this != &source) // ochrona
        {
            delete[] items_;
            items_ = new T[source.size()];
            size_ = source.size_;
            std::copy(source.begin(), source.end(), items_);
        }

        return *this;
    }

    Vector& operator=(Vector&& source)
    {
        //std::cout << "Move operator= of vector" << std::endl;

        if (this != &source) // ochrona
        {
            items_ = source.items_;
            size_ = source.size_;

            source.items_ = nullptr;
            source.size_ = 0;
        }

        return *this;
    }

    ~Vector()
    {
        delete[] items_;
    }

    bool empty() const
    {
        return size_ == 0;
    }

    size_t size() const
    {
        return size_;
    }

    const_iterator begin() const
    {
        return items_;
    }

    const_iterator end() const
    {
        return items_ + size_;
    }

    iterator begin()
    {
        return items_;
    }

    iterator end()
    {
        return items_ + size_;
    }

    T& operator[](size_t index)
    {
        return *(items_ + index);
    }

    const T& operator[](size_t index) const
    {
        return *(items_ + index);
    }
};

template <typename T>
std::ostream& operator<<(std::ostream& out, const Vector<T>& vec)
{
    out << "[ ";
    for(const auto& item : vec)
        out << item << " ";
    out << "]";

    return out;
}

#endif //CLASS_TEMPLATES_VECTOR_HPP
