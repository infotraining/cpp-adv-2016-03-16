#ifndef CLASS_TEMPLATES_VECTOR_HPP
#define CLASS_TEMPLATES_VECTOR_HPP

#include <cstddef>

template <typename T>
class Vector
{
    size_t size_;
    T* items_;
public:
    Vector() : Vector(0)
    {}

    Vector(size_t size, T value = T()) : size_{size}, items_{new T[size_]}
    {
        std::fill(items_, items_ + size_, value);
    }

    template <typename U>
    Vector(std::initializer_list<U> il) : size_{il.size()}, items_{new T[size_]}
    {
        std::copy(il.begin(), il.end(), items_);
    }

    bool empty() const
    {
        return size_ == 0;
    }

    size_t size() const
    {
        return size_;
    }

    const T* begin() const
    {
        return items_;
    }

    T* begin()
    {
        return items_;
    }

    const T* end() const
    {
        return items_ + size_;
    }

    T* end()
    {
        return items_ + size_;
    }
};

#endif //CLASS_TEMPLATES_VECTOR_HPP
