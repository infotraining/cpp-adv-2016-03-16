#include <iostream>
#include "vector.hpp"

using namespace std;

int main()
{
    Vector<int> vec(10);

    cout << "vec: ";
    for(auto& item : vec)
        cout << item << " ";
    cout << endl;
}