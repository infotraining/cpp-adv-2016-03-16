#define CATCH_CONFIG_MAIN

#include <algorithm>
#include "catch.hpp"
#include "../vector.hpp"

using namespace std;

TEST_CASE("vector constructed")
{
    SECTION("using default constructor")
    {
        Vector<int> vec;

        SECTION("size is zero")
        {
            REQUIRE(vec.size() == 0);
        }

        SECTION("is empty")
        {
            REQUIRE(vec.empty());
        }
    }

    SECTION("using constructor with a size as argument")
    {
        Vector<int> vec(3);

        SECTION("size of vector is adjusted")
        {
            REQUIRE(vec.size() == 3);
        }

        SECTION("all items have a default value")
        {
            REQUIRE(all_of(vec.begin(), vec.end(), [](int x) { return x == int();}));
        }
    }

    SECTION("using constructor with a size as argument and a default value")
    {
        Vector<int> vec(3, -1);

        SECTION("size of vector is adjusted")
        {
            REQUIRE(vec.size() == 3);
        }

        SECTION("all items have a given value")
        {
            REQUIRE(all_of(vec.begin(), vec.end(), [](int x) { return x == -1;}));
        }
    }

    SECTION("using constructor with initializer list")
    {
        Vector<int> vec{1, 2, 3};

        SECTION("size is adjusted")
        {
            REQUIRE(vec.size() == 3);
        }

        SECTION("all items are initialized with values from il")
        {
            auto expected = { 1, 2, 3 };

            REQUIRE(equal(vec.begin(), vec.end(), expected.begin()));
        }
    }
}



