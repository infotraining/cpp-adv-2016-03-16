#include "catch.hpp"
#include <algorithm>
#include <vector>
#include <list>
#include <iostream>

using namespace std;

int foo(int x, int y)
{
    return x * y;
}

class Foo
{
public:
    int operator()(int x, int y) const
    {
        return x * y;
    }
};

class MagicLambda_235253235
{
public:
    int operator()(int x, int y)
    {
        return x * y;
    }
};

TEST_CASE("callable")
{
    SECTION("function is callable")
    {
        auto result = foo(2, 3);
        REQUIRE(result == 6);
    }

    SECTION("function ptr is callable")
    {
        int (*ptr_fun)(int, int) = &foo;

        REQUIRE(ptr_fun(2, 3) == 6);
    }

    SECTION("functor is callable")
    {
        Foo f;

        REQUIRE(f(2, 3) == 6);
    }

    SECTION("lambda is callable")
    {
        auto l = [](int x, int y) { return x * y; };

        REQUIRE(l(2, 3) == 6);
    }
}

bool is_even(int x)
{
    return x % 2 == 0;
}

class IsEven
{
public:
    bool operator()(int x) const
    {
        return x % 2 == 0;
    }
};

template <typename T>
struct Greater
{
    bool operator()(const T& a, const T& b) const
    {
        return a > b;
    }
};

template <typename Iterator, typename Predicate>
Iterator my_find_if(Iterator start, Iterator end, Predicate predicate)
{
   Iterator it = start;

    while(it != end)
    {
        if (predicate(*it))
            return it;

        ++it;
    }

    return it;
}

TEST_CASE("my_find_if - simple find algorithm with predicate")
{
    SECTION("finds item when item is present in container")
    {
        vector<int> vec = { 1, 3, 5, 8, 9, 13 };

        auto where = my_find_if(vec.begin(), vec.end(), is_even);

        REQUIRE(where != vec.end());
        REQUIRE(*where == 8);
    }

    SECTION("returns end() when item is not in a container")
    {
        vector<int> vec = { 1, 3, 5, 9, 13 };

        auto where = my_find_if(vec.begin(), vec.end(), is_even);

        REQUIRE(where == vec.end());
    }

    SECTION("works with list and functor")
    {
        list<int> lst = { 1, 3, 5, 8, 9, 13 };

        auto where = my_find_if(lst.begin(), lst.end(), IsEven());

        REQUIRE(*where == 8);
    }

    SECTION("works with list and with lambda")
    {
        list<int> lst = { 1, 3, 5, 8, 9, 13 };

        auto where = my_find_if(lst.begin(), lst.end(), [](int x) { return x % 2 == 0; });

        REQUIRE(*where == 8);
    }
}

class Person
{
    int id_;
    string name_;
public:
    Person(int id, const string& name) : id_{id}, name_{name}
    {
    }

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }
};

TEST_CASE("sort")
{
    vector<int> vec = { 6, 2, 7, 8, 234, 234, 122, 5, 1, 0, 99 };

    SECTION("default ascending")
    {
        sort(vec.begin(), vec.end());

        REQUIRE(is_sorted(vec.begin(), vec.end()));
    }

    SECTION("descending with functor")
    {
        sort(vec.begin(), vec.end(), Greater<int>());

        REQUIRE(is_sorted(vec.rbegin(), vec.rend()));
        REQUIRE(is_sorted(vec.begin(), vec.end(), greater<int>()));
    }
}

TEST_CASE("Container of people")
{
    vector<Person> people = { Person{1, "Kowalski"}, Person{2, "Nowak"},
                              Person{3, "Anonim"}, Person{4, "Kowalska"} };

    SECTION("can be sorted with lamba")
    {
        sort(people.begin(), people.end(),
             [](const Person& p1, const Person& p2) { return p1.name() < p2.name();});

        REQUIRE(people.front().name() == "Anonim");
        REQUIRE(people.back().name() == "Nowak");
    }

    SECTION("can use my_find_if")
    {
        auto where = my_find_if(people.begin(), people.end(),
                                [](const Person& p) { return p.name() == "Kowalska";});

        REQUIRE(where->id() == 4);
    }
}

TEST_CASE("counting")
{
    list<int> lst = { 1, 3, 5, 8, 9, 13 };

    SECTION("count")
    {
        REQUIRE(count(lst.begin(), lst.end(), 8) == 1);
    }

    SECTION("count_if")
    {
        REQUIRE(count_if(lst.begin(), lst.end(), [](int x) { return x % 2 == 0;}) == 1);
    }
}

TEST_CASE("copying")
{
    vector<int> vec = { 1, 2, 3, 5, 6, 8, 9 };

    SECTION("copy")
    {
        vector<int> dest(vec.size());

        copy(vec.begin(), vec.end(), dest.begin());
    }

    SECTION("copy_if")
    {
        vector<int> dest;

        auto is_even = [](int x) { return x % 2 == 0;};

        copy_if(vec.begin(), vec.end(), back_inserter(dest), is_even);

        REQUIRE(all_of(dest.begin(), dest.end(), is_even));
    }
}

TEST_CASE("removing")
{
    auto is_even = [](int x) { return x % 2 == 0;};

    SECTION("items in vector")
    {
        vector<int> vec = { 1, 2, 3, 5, 6, 8, 9 };

        SECTION("items using predicate")
        {
            auto garbage_begin = remove_if(vec.begin(), vec.end(), is_even);
            vec.erase(garbage_begin, vec.end());

            cout << "\nitems: ";
            for(const auto& item : vec)
                cout << item << " ";
            cout << endl;
        }
    }

    SECTION("items in list")
    {
        list<int> lst = { 1, 2, 3, 5, 6, 8, 9 };

        SECTION("items using predicate")
        {
            lst.remove_if(is_even);
        }
    }
}

TEST_CASE("partition")
{
    vector<Person> people = { Person{1, "Kowalski"}, Person{2, "Nowak"},
                              Person{3, "Anonim"}, Person{4, "Kowalska"} };

    auto boundary = stable_partition(people.begin(), people.end(), [](const Person& p) { return p.id() % 2 == 0;});

    auto index = distance(people.begin(), boundary);

    vector<int> ids(people.size());
    transform(people.begin(), people.end(), ids.begin(), [](const Person& p) { return p.id(); });

    auto expected_even_ids = { 2, 4 };
    auto expected_odd_ids = { 1, 3 };

    for(const auto& item : ids)
        cout << item << " ";
    cout << endl;

    REQUIRE(equal(ids.begin(), ids.begin() + index, expected_even_ids.begin()));
    REQUIRE(equal(ids.begin() + index, ids.end(), expected_odd_ids.begin()));
}

TEST_CASE("remove all items greater than avg")
{
    vector<int> vec = { 6, 2, 7, 8, 234, 234, 122, 5, 1, 0, 99 };

    //auto avg = accumulate(vec.begin(), vec.end(), 0.0) / vec.size();
    double sum = 0.0;
    for_each(vec.begin(), vec.end(), [&](int x) { sum += x; });

    auto avg = sum / vec.size();
    auto factor = 2.0;
    auto predicate = [=](int x) { return x > avg * factor;};

    factor = 3.0; // without impact on algorithm
    auto garbage_begin = remove_if(vec.begin(), vec.end(), predicate);

    vec.erase(garbage_begin, vec.end());
}
