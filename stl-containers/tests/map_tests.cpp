#include "catch.hpp"

#include <map>
#include <string>
#include <algorithm>

using namespace std;

TEST_CASE("map", "[map]")
{
    map<int, string> dict = { {1, "one"}, {2, "two"}, {7, "seven"}, {4, "four"} };

    SECTION("stores pairs <key, value>")
    {
        dict.insert(pair<int, string>(0, "zero"));
        dict.insert(make_pair(5, "five"));
    }

    SECTION("stores items in order (ascending) by keys")
    {
        vector<int> keys(dict.size());

        transform(dict.begin(), dict.end(), keys.begin(),
                  [](auto& kv) { return kv.first; });

        auto expected = { 1, 2, 4, 7 };

        REQUIRE(equal(keys.begin(), keys.end(), expected.begin()));
    }

    SECTION("is indexed by key")
    {
        auto number = dict[1];

        REQUIRE(number == "one");

        SECTION("when index is not present - default value is inserted")
        {
            number = dict[10];

            REQUIRE(number == "");

            SECTION("key is inserted with default value")
            {
                vector<int> keys(dict.size());

                transform(dict.begin(), dict.end(), keys.begin(),
                          [](auto& kv) { return kv.first; });

                auto expected = { 1, 2, 4, 7, 10 };

                REQUIRE(equal(keys.begin(), keys.end(), expected.begin()));
            }

        }
    }

    SECTION("at() - throws when key is not present")
    {
        SECTION("when key exists - returns ref to value")
        {
            string number = dict.at(7);
            REQUIRE(number == "seven");
        }

        SECTION("when key doesn't exist - throws exception")
        {
            REQUIRE_THROWS_AS(dict.at(10), std::out_of_range);
        }
    }
}

TEST_CASE("map - common interface - find, count, erase")
{
    map<int, string> dict = { {1, "one"}, {2, "two"}, {7, "seven"}, {4, "four"} };

    SECTION("can find item by key")
    {
        auto it = dict.find(7);

        REQUIRE(it != dict.end());
        REQUIRE(it->second == "seven");
    }

    SECTION("if key doesn't exist")
    {
        auto it = dict.find(13);

        REQUIRE(it == dict.end());
    }

    SECTION("can count items by key")
    {
        REQUIRE(dict.count(7) == 1);
        REQUIRE(dict.count(13) == 0);
    }

    SECTION("can remove item by key")
    {
        dict.erase(7);

        REQUIRE(dict.count(7) == 0);
    }
}

TEST_CASE("map sorted in descending order", "[map]")
{
    map<string, int, greater<string>> dict_desc =
        { {"one", 1} ,{"four", 4}, {"five", 5} };

    SECTION("stores keys in desc")
    {
        auto expected = { "one", "four", "five" };

        vector<string> keys(dict_desc.size());

        transform(dict_desc.begin(), dict_desc.end(), keys.begin(),
                  [](auto& kv) { return kv.first; });

        REQUIRE(equal(keys.begin(), keys.end(), expected.begin()));
    }
}
