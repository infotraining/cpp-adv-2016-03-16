#include <deque>
#include "catch.hpp"

using namespace std;

TEST_CASE("deque", "[dequeue]")
{
    deque<int> dq = { 1, 2, 3};
    auto size_before = dq.size();


    SECTION("can insert items at the front")
    {
        dq.push_front(0);

        REQUIRE(dq.front() == 0);
        REQUIRE(dq.size() == size_before + 1);
    }
}
