#define CATCH_CONFIG_MAIN

#include <algorithm>
#include <vector>
#include <list>
#include "catch.hpp"

using namespace std;

TEST_CASE("vector - default construction", "[vector]")
{
    vector<int> vec;

    SECTION("after creation is empty")
    {
        REQUIRE(vec.empty());
    }

    SECTION("size is zero")
    {
        REQUIRE(vec.size() == 0);
    }

    SECTION("capacity is greater or equal zero")
    {
        REQUIRE(vec.capacity() >= 0);
    }
}

TEST_CASE("vector - init with size", "[vector]")
{
    vector<int> vec(3);

    SECTION("size is adjusted")
    {
        REQUIRE(vec.size() == 3);
    }

    SECTION("capacity is greater or equal size")
    {
        REQUIRE(vec.capacity() >= vec.size());
    }

    SECTION("all items have default value")
    {
//        for(auto item : vec)
//            REQUIRE(item == 0);

        REQUIRE(all_of(vec.begin(), vec.end(), [](int x) { return x == 0; }));
    }
}

TEST_CASE("vector - init with il", "[vector]")
{
    vector<int> vec = { 1, 2, 3 };

    SECTION("size is adjusted")
    {
        REQUIRE(vec.size() == 3);
    }

    SECTION("all items have asigned value from il")
    {
        auto expected = { 1, 2, 3 };

        REQUIRE(equal(vec.begin(), vec.end(), expected.begin(), expected.end()));
    }
}

class Fixture
{
public:
    vector<int> vec = { 1, 2, 3, 4 };
};

TEST_CASE("vector - adding items at the end", "[vector]")
{
    vector<int> vec = { 1, 2, 3, 4 };

    SECTION("push_back()")
    {
        size_t size_before = vec.size();
        size_t capacity_before = vec.capacity();
        vec.push_back(5);

        SECTION("item is inserted at the end")
        {
            REQUIRE(vec.back() == 5);
            REQUIRE(vec[4] == 5);
        }

        SECTION("size is increased")
        {
            REQUIRE(vec.size() == size_before + 1);
        }

        SECTION("capacity is increased")
        {
            REQUIRE(vec.capacity() > capacity_before);
        }
    }
}

TEST_CASE("vector - optimal init")
{
    vector<int> vec;

    SECTION("after reserve()")
    {
        vec.reserve(256);

        SECTION("vec is empty")
        {
            REQUIRE(vec.empty());
        }

        SECTION("capacity is adjusted")
        {
            REQUIRE(vec.capacity() == 256);
        }

        SECTION("after insert")
        {
            for(int i = 0; i < 128; ++i)
                vec.push_back(i);

            SECTION("size is adjusted")
            {
                REQUIRE(vec.size() == 128);
            }

            SECTION("capacity doesn't change")
            {
                REQUIRE(vec.capacity() == 256);
            }

            SECTION("shrink_to_fit() frees memory")
            {
                vec.shrink_to_fit();

                REQUIRE(vec.size() == vec.capacity());
            }
        }
    }
}

TEST_CASE("vector - insert", "[vector]")
{
    vector<int> vec = { 1, 2, 3, 4, 5, 6, 7 };

    SECTION("at the begin")
    {
        vec.insert(vec.begin(), 0);

        REQUIRE(vec[0] == 0);
        REQUIRE(vec.front() == 0);
    }

    SECTION("in the midle")
    {
        auto pos = vec.begin() + 3;
        vec.insert(pos, 0);

        auto expected = { 1, 2, 3, 0, 4, 5, 6, 7 };

        REQUIRE(equal(vec.begin(), vec.end(), expected.begin()));
    }

    SECTION("many items")
    {
        list<int> lst = { 1, 2, 3 };

        vec.insert(vec.begin(), lst.begin(), lst.end());

        auto expected = { 1, 2, 3, 1, 2, 3, 4, 5, 6, 7 };

        REQUIRE(equal(vec.begin(), vec.end(), expected.begin()));
    }
}

TEST_CASE("vector - erase", "[vector]")
{
    vector<int> vec = { 1, 2, 3, 4, 5, 6 };

    SECTION("removes items")
    {
        vec.erase(vec.begin() + 1, vec.end() - 1);

        REQUIRE(vec.size() == 2);

        auto expected = {1, 6};
        REQUIRE(equal(vec.begin(), vec.end(), expected.begin()));
    }
}

void legacy_code(int* tab, unsigned int size)
{

}

TEST_CASE("vector - works with legacy API", "[vector]")
{
    vector<int> vec = { 1, 2, 3, 4, 5, 6 };

    legacy_code(vec.data(), vec.size());
}
