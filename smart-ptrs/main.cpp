#include <iostream>
#include <string>
#include <memory>
#include <cassert>
#include <vector>

using namespace std;

class Gadget
{
    int id_ = 0;
    string name_ = "unknown";
public:
    Gadget()
    {
        cout << "Gadget-cctor(id: " << id_ << ", name: " << name_ << ")" << endl;
    }

    Gadget(int id, const string& name) : id_{id}, name_{name}
    {
        cout << "Gadget-cctor(id: " << id_ << ", name: " << name_ << ")" << endl;
    }

    ~Gadget()
    {
        cout << "~Gadget(id: " << id_ << ", name: " << name_ << ")" << endl;
    }

    virtual void use()
    {
        cout << "using ";
        info();
    }

    void info() const
    {
        cout << "Gadget(id: " << id_ << ", name: " << name_ << ")" << endl;
    }
};

class SuperGadget : public Gadget
{
    string extra_;
public:
    SuperGadget(int id, const string& name, const string& extra) : Gadget{id, name}, extra_{extra}
    {
    }

    void use() override
    {
        Gadget::use();
        cout << "Extra stuff: " << extra_ << endl;
    }
};

void use_by_ptr(Gadget* g)
{
    if (g)
        g->use();
}

void use_by_ref(Gadget& g)
{
    g.use();
}

unique_ptr<Gadget> foxconn_factory(const string& name)
{
    static int id_gen;

    auto g = make_unique<Gadget>(++id_gen, name);

    return g;
}

void use_and_destroy(unique_ptr<Gadget> g)
{
    g->use();
}

int main(int argc, char *argv[])
{
    {
        unique_ptr<Gadget> uptr1(new Gadget(1, "ipad"));
        uptr1->info();
        (*uptr1).info();
    }

    cout << "\n\n";

    // C++14 - Visual Studio 2013
    {
        unique_ptr<Gadget> uptr1 = make_unique<Gadget>(1, "mp3 player");
        uptr1->info();
        use_by_ptr(uptr1.get());
        use_by_ref(*uptr1);
    }

    cout << "\n\n";

    // bad code - memory leak
    {
       use_by_ptr(new Gadget(2, "iphone"));
    }

    cout << "\n\n";

    // unique_ptr - nocopyable
    {
        unique_ptr<Gadget> uptr1 = foxconn_factory("mp5 player");
        unique_ptr<Gadget> uptr2 = move(uptr1);

        assert(uptr1 == nullptr);
        uptr2->info();
    }

    cout << "\n\n";

    // unique_ptr - sink function
    {
        unique_ptr<Gadget> uptr1 = foxconn_factory("mp6 player");

        use_and_destroy(move(uptr1));
        use_and_destroy(foxconn_factory("mp7 player"));
    }

    cout << "\n\n";

    // unique_ptr and vector
    {
        vector<unique_ptr<Gadget>> gadgets;

        gadgets.push_back(make_unique<Gadget>(1, "mp1 player"));
        gadgets.push_back(foxconn_factory("mp2 player"));

        auto g = foxconn_factory("mp3 player");
        gadgets.push_back(move(g));
    }

    cout << "\n\n";

    // unique_ptr as dynamic array
    {
        unique_ptr<Gadget[]> gadgets = make_unique<Gadget[]>(100);

        gadgets[19].use();
    }

    cout << "\n\n";

    //
    {
        shared_ptr<Gadget> sp_gadget;

        {
            vector<shared_ptr<Gadget>> gadgets;

            gadgets.push_back(make_shared<Gadget>(1, "mp1"));
            gadgets.push_back(foxconn_factory("mp2"));
            gadgets.push_back(make_shared<SuperGadget>(8, "mp8", "ping"));
            gadgets.push_back(foxconn_factory("mp4"));

            sp_gadget = gadgets[2];

            for(const auto& g : gadgets)
                g->use();

            cout << "REF COUNTER: " << sp_gadget.use_count() << endl;
        }
        cout << "\n\n";

        if (sp_gadget)
            sp_gadget->use();

        cout << "***" << endl;
    }
}
