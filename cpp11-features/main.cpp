#include <iostream>
#include <vector>
#include <set>
#include <array>

using namespace std;

void foo(int* ptr)
{
    cout << "foo(int*)" << endl;
}

void foo(int value)
{
    cout << "foo(int)" << endl;
}

void foo(nullptr_t)
{
    cout << "foo(nullptr_t)" << endl;
}

int main(int argc, char *argv[])
{
    int* old_style_ptr = NULL;
    int* ptr = nullptr; // C++11
    int* ptr2{}; // the same as line above

    if (ptr == nullptr)
        foo(nullptr);

    if (!ptr2)
        foo(ptr2);

    // auto

    auto x = 10;  // int x = ...
    auto y(x * 3.14);  // double y = ...

    // auto jako iterator
    multiset<int, greater<int>> unique_numbers
            = { 1, 6, 34, 2, 345, 2, 1, 1, 34, 2, 6, 764, 354, 6 };

    unique_numbers.insert(13);

    auto it = unique_numbers.begin();
    while(it != unique_numbers.end())
    {
        cout << *it << " ";
        ++it;
    }
    cout << endl;

    // range based for
    for(auto item : unique_numbers)
        cout << item << " ";
    cout << endl;

    for(auto it = unique_numbers.begin(); it != unique_numbers.end(); ++it)
    {
        auto item = *it;
        cout << item << " ";
    }
    cout << endl;

    vector<int> vec = { 1, 2, 3, 4, 5 };

    for(auto& item : vec)
        ++item;

    for(auto item : vec)
        cout << item << " ";
    cout << endl;

    vector<string> words = { "one", "two", "three" };

    for(const auto& w : words)
        cout << w << " ";
    cout << endl;

    // range-based-for with native arrays
    int tab[10] = { 1, 2, 3, 4, 5, 6, 7 };
    array<int, 10> tab1 = { 1, 2, 3, 4, 5, 6, 7 };

    for(const auto& item : tab)
        cout << item << " ";
    cout << endl;

    for(auto it = begin(tab); it != end(tab); ++it)
        cout << *it << " ";

    // range-based-for with il
    for(auto item : { 1, 2, 3 })
    {
        if (unique_numbers.count(item))
            cout << item << " is in container" << endl;
    }
}
