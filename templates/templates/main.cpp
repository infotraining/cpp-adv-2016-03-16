#include <iostream>
#include <cstring>

using namespace std;

template <typename T>
const T& maximum(const T& a, const T& b)
{
    cout << "maximum(T, T)" << endl;
    return a < b ? b : a;
}

template <typename T>
T* maximum(T* a, T* b)
{
    cout << "maximum(T*, T*)" << endl;
    return *a < *b ? b : a;
}

template <>
const char* maximum(const char* txt1, const char* txt2)
{
    cout << "maximum(const char*, const char*)" << endl;
    return std::strcmp(txt1, txt2) < 0 ? txt2 : txt1;
}

template <typename T>
class Pair
{
    T items_[2];
public:
    Pair(const T& first, const T& second)
    {
        items_[0] = first;
        items_[1] = second;
    }

    const T& max() const
    {
        return items_[0] < items_[1] ? items_[1] : items_[0];
    }
};

template <typename T>
class Pair<T*>
{
    T* first_;
    T* second_;
public:
    Pair(T* first, T* second) : first_{first}, second_{second}
    {
    }

    const T* max() const
    {
        return *first_ < *second_ ? second_ : first_;
    }
};

template <>
class Pair<const char*>
{
    const char* first_;
    const char* second_;
public:
    Pair(const char* first, const char* second) : first_{first}, second_{second}
    {
    }

    const char* max() const
    {
        return std::strcmp(first_, second_) < 0 ? second_ : first_;
    }
};

int main(int argc, char *argv[])
{
    int a = 10;
    int b = 20;

    int result = maximum(a, b);

    cout << "result for int: " << result << endl;

    string txt1 = "ala";
    string txt2 = "ola";

    cout << "result for string: " << maximum(txt1, txt2) << endl;

    cout << "maximum(1, 2.3) = " << maximum<double>(1, 2.3) << endl;

    string* ptr_result = maximum(&txt1, &txt2);

    cout << "*result = " << *ptr_result << endl;

    const char* str1 = "ala";
    const char* str2 = "aza";

    auto str_result = maximum(str1, str2);
    cout << "maximum(str1, str2) = " << str_result << endl;

    Pair<int> pair_int(1, 6);
    cout << pair_int.max() << endl;

    Pair<int*> pair_ptr_int(&a, &b);
    cout << *pair_ptr_int.max() << endl;

    const Pair<const char*> pair_cstr(str1, str2);
    cout << pair_cstr.max() << endl;

}
